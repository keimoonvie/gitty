#!/usr/bin/env python
from setuptools import *
import shutil
import os

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "gitty",
    version = "0.1",
    packages = find_packages(),

    author = "KeiMOon",
    author_email = "keimoonvie@gmail.com",
    description = "simple git reposities management software",
    long_description = read("README.md"),

    license = "GPL",
    keywords = "git scm version-control ssh https",
    url = "https://github.com/keimoon/gitty/",

    entry_points = {
        'console_scripts': [
            'gitty-shell = gitty.shell:main',
            'gitty-hook = gitty.hook:main',
            'gitty = gitty.gitty:main',
            ],
        },
)

config_file = os.path.join(os.path.dirname(__file__), 'gitty.conf')
shutil.copy(config_file, '/etc/')
