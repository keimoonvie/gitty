Gitty: simple git administration tool
=====================================

Gitty is an administration tool for git versioning control system. This 
project aims for small - medium development teams (lesser than 50 people).

Features
--------

- Reposity management.
- User and group management.
- Access control based on user and group.
- SSH keys management (an user can have multiple SSH keys).
- Cloning, pushing and pulling via HTTPS (authentication by password).

Requirement
-----------

- Gitty is tested on Centos 6.x. It can run on Ubuntu or other distros but needs 
configuration.
- Apache httpd server is required for HTTPS git.
- Apache mod_suexec, mod_ssl.
- Python 2.6/2.7 with setuptools
- git (with git-http-backend).

Installation
------------

### Add an user for git

On centos:

    useradd git
    su git
    mkdir ~/.ssh
    chmod 600 ~/.ssh

### Configure Apache virtual host

You should have a dedicated virtual host for having git over HTTPS.
The virtual host configuration sample is provided in _extras/gitty-virtualhost.conf_.

By default, gitty root is _/var/www/gitty_. Reposities are stored in _/var/www/gitty/repos_, 
which is also the document root for the virtual host. Every HTTPS request to this virtual 
host will be redirected to a CGI script: _git-http-backend-wrapper_, which you should copy
from _extras_ to _/var/www/gitty/cgi_. You also must copy your own _git-http-backend_ script
(by default, it is in _/usr/libexec/git-core_) to the same directory as _git-http-backend-wrapper_.

The virtual host is Suexec to user _git_, group _git_, so Apache server should have mod_suexec
enabled.

For HTTPS, you must generate your own key, and put in _etc/ssl/server.crt_ and _etc/ssl/server.key_
(or you can put them in other location, and change the virtual host config file).

### Install gitty

Clone this repo, _python setup.py install_, this will install _gitty_ and _gitty_shell_ to _/usr/bin_
and _gitty.conf_ to _/etc_.

**If you want to customize gitty data directory, you must change configurations in _gitty.conf_.**

Basic usage
-----------

Type _gitty_ to list all available commands.

### Adding user:
    gitty add-user example_user user@example.com /path/to/public_key/example_user.pub
    Password:
    Retype Password:

### Adding group
    gitty add-group example_group 'An example group'

### Adding reposity
    gitty add-repo example_repo 'Example repo' 'A description'

### Adding user to a reposity
    gitty add-user-repo example_user example_repo
    /etc/init.d/httpd reload

### Adding group to a reposity
    gitty add-group-repo example_group example_repo
    /etc/init.d/httpd reload

### Cloning via SSH
    git clone git@gitty.example.com:example_repo

### Cloning via HTTPS:
    git clone https://gitty.example.com/example_repo.git
    Password:

