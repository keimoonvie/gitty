#!/usr/bin/env python
import sys
import os
import util
import config
import repo

def check_cmd(user, cmd):
    pieces = cmd.split(' ')
    if len(pieces) != 2 or not pieces[0] in ('git-receive-pack', 'git-upload-pack'):
        sys.stderr.write('Wrong command: %s\n' % pieces[0])
        sys.exit(1)

    repo_name = pieces[1].strip("'")
    if repo_name[-4:len(repo_name)] == '.git':
        repo_name = repo_name[-4:len(repo_name)]

    if not (repo.exist(repo_name)):
        sys.stderr.write("Repo does not exist: %s\n" % repo_name)
        sys.exit(1)

    if not (repo.check_user_repo(user, repo_name)):
        sys.stderr.write("Permission denied for %s\n" % user)
        sys.exit(1)

    repo_path = repo.get_repo_path(repo_name)
    return "%s '%s'" % (pieces[0], repo_path)
    
    
def main():
    config.config_init()
    if len(sys.argv) < 2:
        sys.stderr.write("Wrong arguments")
        sys.exit(1)
    
    cmd = os.environ.get('SSH_ORIGINAL_COMMAND', None)
    if (cmd == None):
        sys.stderr.write("Cannot find SSH_ORIGINAL_COMMAND\n")
        sys.exit(1)

    user = sys.argv[1]
    newcmd = check_cmd(user, cmd)
    os.execvp('git', ['git', 'shell', '-c', newcmd])
    
