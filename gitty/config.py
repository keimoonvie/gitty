import sys
import os
from ConfigParser import ConfigParser

config = ConfigParser()

def config_init():
    config_file = '/etc/gitty.conf'
    if not (os.path.exists(config_file)):
        sys.stderr.write('Cannot find config file %s\n' % config_file)
        sys.exit(1)
    config.read(config_file)

def get_config(group, conf):
    return config.get(group, conf)
