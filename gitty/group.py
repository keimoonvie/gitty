import os
import sys
import util
import user
import repo
import http
from config import get_config

def load_group(group_name):
    group_file = get_config('gitty', 'gitty_root') + '/data/groups'
    f = open(group_file, 'r')
    util.lock_read(f)

    data = util.json_load(f)

    if not (group_name in data):
        sys.stderr.write("Group name not existed\n")
        sys.exit(1)

    util.unlock_file(f)
    f.close()

    return data[group_name]

def exist(group_name):
    group_file = get_config('gitty', 'gitty_root') + '/data/groups'
    f = open(group_file, 'r')
    util.lock_read(f)
    data = util.json_load(f)
    util.unlock_file(f)
    f.close()

    return group_name in data

def add_group(group_name, display_name):
    if not(util.verify_name(group_name)):
        return (False, "Group name not correct\n")
        
    group_file = get_config('gitty', 'gitty_root') + '/data/groups'
    f = open(group_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    if group_name in data:
        return (False, "Group name existed\n")

    data[group_name] = {'group_name': group_name, 'display_name': display_name}

    util.json_dump(f, data)

    util.unlock_file(f)
    f.close()
    return (True, "")

def edit_group(group_name, display_name):
    group_file = get_config('gitty', 'gitty_root') + '/data/groups'
    f = open(group_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    if not(group_name in data):
        return (False, "Group name not existed\n")

    data[group_name]['display_name'] = display_name

    util.json_dump(f, data)

    util.unlock_file(f)
    f.close()
    return (True, "")

def add_user_to_group(username, group_name):
    if not(user.exist(username)):
        return (False, "Username not existed\n")

    group_file = get_config('gitty', 'gitty_root') + '/data/groups'
    f = open(group_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    if not(group_name in data):
        return (False, "Group name not existed\n")

    if not('users' in data[group_name]):
        data[group_name]['users'] = []

    if not(username in data[group_name]['users']):
        data[group_name]['users'].append(username)
    else:
        return (False, "User is already in group\n")

    http.rewrite_htgroup_file(data)
    user.add_group(username, group_name)

    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()
    return (True, "");

def delete_user_from_group(username, group_name):
    if not(user.exist(username)):
        return (False, "Username not existed\n")

    group_file = get_config('gitty', 'gitty_root') + '/data/groups'
    f = open(group_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    if not(group_name in data):
        return (False, "Group name not existed\n")

    if not('users' in data[group_name]):
        return (False, "User is not in group\n")

    if not(username in data[group_name]['users']):
        return (False, "User is not in group\n")
    else:
        data[group_name]['users'].remove(username)

    http.rewrite_htgroup_file(data)
    user.delete_group(username, group_name)

    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()
    return (True, "")
    
def add_repo(group_name, repo_name):
    group_file = get_config('gitty', 'gitty_root') + '/data/groups'
    f = open(group_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    if not(group_name in data):
        return (False, "Group name not existed\n")

    if not('repos' in data[group_name]):
        data[group_name]['repos'] = []

    if not(repo_name in data[group_name]['repos']):
        data[group_name]['repos'].append(repo_name)

    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()

    return (True, "")

def delete_repo(group_name, repo_name):
    group_file = get_config('gitty', 'gitty_root') + '/data/groups'
    f = open(group_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    if not(group_name in data):
        return (False, "Group name not existed\n")

    if not('repos' in data[group_name]):
        return (False, "")

    if repo_name in data[group_name]['repos']:
        data[group_name]['repos'].remove(repo_name)

    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()
    return (True, "")

def delete_user_all(username, groups):
    group_file = get_config('gitty', 'gitty_root') + '/data/groups'
    f = open(group_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    for group_name in groups:
        if not(group_name in data):
            continue
            
        if not('users' in data[group_name]):
            continue

        if username in data[group_name]['users']:
            data[group_name]['users'].remove(username)        

    http.rewrite_htgroup_file(data)

    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()

    return (True, "")

def delete_repo_all(repo_name, groups):
    group_file = get_config('gitty', 'gitty_root') + '/data/groups'
    f = open(group_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)

    for group_name in groups:
        if not(group_name in data):
            continue

        if not('repos' in data[group_name]):
            continue

        if repo_name in data[group_name]['repos']:
            data[group_name]['repos'].remove(repo_name)

    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()

    return (True, "")

def delete_group(group_name):
    group_file = get_config('gitty', 'gitty_root') + '/data/groups'
    f = open(group_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    if not(group_name in data):
        sys.stderr.write("Group name not existed\n")
        sys.exit(1)

    if 'users' in data[group_name] and len(data[group_name]['users']) > 0:
        user.delete_group_all(group_name, data[group_name]['users'])

    if 'repos' in data[group_name] and len(data[group_name]['repos']) > 0:
        repo.delete_group_all(group_name, data[group_name]['repos'])

    del data[group_name]
    http.rewrite_htgroup_file(data)

    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()

    return (True, "")

def view_list(start, end):
    group_file = get_config('gitty', 'gitty_root') + '/data/groups'
    f = open(group_file, 'r')
    util.lock_read(f)

    data = util.json_load(f)
    keys = data.keys()
    keys.sort()
    if end == -1:
        end = len(keys)
    elif end < -1:
        end = end + 1
    keys = keys[start:end]
    for key in keys:
        print "%s\t%s" %(data[key]['group_name'], data[key]['display_name'])

    util.unlock_file(f)
    f.close()

def view_detail(name):
    group_file = get_config('gitty', 'gitty_root') + '/data/groups'
    f = open(group_file, 'r')
    util.lock_read(f)

    data = util.json_load(f)
    if not name in data:
        sys.stderr.write("Cannot find group")
        sys.exit(1)

    group_data = data[name]
    print "Group name: %s" % group_data['group_name']
    print "Display name: %s" % group_data['display_name']
    if not('users' in group_data) or len(group_data['users']) == 0:
        print "No user"
    else:
        print "Users: %s" % ", ".join(group_data['users'])
    if not('repos' in group_data) or len(group_data['repos']) == 0:
        print "No repo"
    else:
        print "Repos: %s" % ", ".join(group_data['repos'])

    util.unlock_file(f)
    f.close()
