import sys

class Command:
    def __init__(self):
        pass

    def run(self):
        pass

class CommandFactory:
    def __init__(self):
        self.command_map = {}

    def add_command(self, command_name, command_obj, command_desc):
        self.command_map[command_name] = {'obj':command_obj, 'desc':command_desc}

    def print_help(self):
        print 'Available command:'
        for command_name, cmd in self.command_map.iteritems():
            print ' - %s\t\t%s' % (command_name, cmd['desc'])

    def run(self):
        if len(sys.argv) < 2 or not(sys.argv[1] in self.command_map):
            self.print_help()
            sys.exit(2)
        else:
            self.command_map[sys.argv[1]]['obj'].run()
