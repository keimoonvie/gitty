import os
import sys
from gitty.command import Command
from gitty import repo
from gitty import util

class DeleteUserRepoCommand(Command):
    def run(self):
        if len(sys.argv) < 4:
            print "Usage: %s %s [username] [repo_name]" % (sys.argv[0], sys.argv[1])
            sys.exit(2)

        username = sys.argv[2]
        repo_name = sys.argv[3]
        repo.delete_user_from_repo(username, repo_name)
        print "Deleted user %s from repo %s" % (username, repo_name)
        sys.exit(0)
