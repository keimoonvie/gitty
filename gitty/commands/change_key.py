import os
import sys
from gitty.command import Command
from gitty import user
from gitty import ssh
from gitty import util
from gitty.config import get_config

class ChangeKeyCommand(Command):
    def run(self):
        if len(sys.argv) < 4:
            print "Usage: %s %s [username] [keyname]" % (sys.argv[0], sys.argv[1])
            sys.exit(2)
        username = sys.argv[2]
        keyname = sys.argv[3]

        if not(user.exist(username)):
            sys.stderr.write("User not existed\n")
            sys.exit(1)

        keyfile = util.read_interactive('Keyfile (leave empty to cancel): ')
        if len(keyfile) == 0:
            print "SSH key not changed"
            sys.exit(0)

        ssh.change_key(username, keyname, keyfile)
        print "SSH key changed successfully"
        sys.exit(0)
