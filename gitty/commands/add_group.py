import os
import sys
from gitty.command import Command
from gitty import group
from gitty import util

class AddGroupCommand(Command):
    def run(self):
        if len(sys.argv) < 3:
            self.interactive()
        else:
            self.command_line()
        self.execute()

    def interactive(self):
        self.group_name = util.read_interactive("Group name: ")
        self.display_name = util.read_interactive("Display name: ")

    def command_line(self):
        if len(sys.argv) < 4:
            print "Usage: %s %s [group_name] [Display name]" % (sys.argv[0], sys.argv[1])
            sys.exit(2)

        self.group_name = sys.argv[2]
        self.display_name = sys.argv[3]

    def execute(self):
        group.add_group(self.group_name, self.display_name)
        print "Group %s added successfully" % self.group_name
        sys.exit(0)
