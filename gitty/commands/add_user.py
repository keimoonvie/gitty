import os
import sys
from gitty.command import Command
from gitty import user
from gitty import util

class AddUserCommand(Command):
    def run(self):
        if len(sys.argv) < 3:
            self.interactive()
        else:
            self.command_line()
        self.execute()

    def interactive(self):
        self.username = util.read_interactive("Username: ")
        self.email = util.read_interactive("Email: ")
        self.keyfile = util.read_interactive("Key file (leave empty for not using ssh key): ");
        self.password = util.read_password("Password: ")
        self.password_retype = util.read_password("Retype password: ")
        
    def command_line(self):
        if len(sys.argv) < 5:
            print "Usage: %s %s [username] [email] [keyfile]" % (sys.argv[0], sys.argv[1])
            sys.exit(2)

        self.username = sys.argv[2]
        self.email = sys.argv[3]
        self.keyfile = sys.argv[4]

        self.password = util.read_password("Password: ")
        self.password_retype = util.read_password("Retype password: ")

    def execute(self):
        user.add_user(self.username, self.email, self.keyfile, self.password, self.password_retype)
        print "User %s added successfully" % self.username
        sys.exit(0)
