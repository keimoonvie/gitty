import os
import sys
from gitty.command import Command
from gitty import repo
from gitty import util

class DeleteRepoCommand(Command):
    def run(self):
        if len(sys.argv) < 3:
            print "Usage: %s %s [repo_name]" % sys.argv[0]
            sys.exit(2)

        repo_name = sys.argv[2]
        repo.delete_repo(repo_name)
        print "Deleted repo %s" % repo_name
        sys.exit(0)
