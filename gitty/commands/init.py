import sys
from gitty.command import Command
from gitty import util
from gitty import ssh
from gitty.config import get_config

class InitCommand(Command):
    def run(self):
        gitty_root = get_config('gitty', 'gitty_root')
        gitty_user = get_config('gitty', 'gitty_user')
        gitty_group = get_config('gitty', 'gitty_group')
        response = util.read_interactive('Warning: this command will delete all existing repos, do you want to initialize? [y/N]: ')
        if not(response == 'y' or response == 'Y'):
            print 'Quiting'
            sys.exit(0)

        print 'Initializing gitty in %s' % gitty_root

        print "====> Creating repo dir"
        gitty_repo_root = gitty_root + '/repos'
        util.create_or_clean_dir(gitty_repo_root)

        print "====> Creating data dir"
        gitty_data_root = gitty_root + '/data'
        util.create_or_clean_dir(gitty_data_root)

        print "====> Creating user database"
        user_file = gitty_data_root + '/users'
        util.recreate_file(user_file, '{}')

        print "====> Creating group database"
        group_file = gitty_data_root + "/groups"
        util.recreate_file(group_file, '{}')

        print "====> Creating http user file"
        ht_user_file = gitty_data_root + "/htusers"
        util.recreate_file(ht_user_file, '')

        print "====> Creating http repo config folder"
        ht_repo_dir = gitty_data_root + "/htrepos"
        util.create_or_clean_dir(ht_repo_dir)        

        print "====> Creating http group file"
        ht_group_file = gitty_data_root + "/htgroups"
        util.recreate_file(ht_group_file, '')

        print "====> Creating repo database"
        repo_file = gitty_data_root + "/repos"
        util.recreate_file(repo_file, '{}')

        print "====> Clean user authorized key"
        authorized_file = ssh.get_authorized_file(gitty_user)
        util.recreate_file(authorized_file, "")
        util.chmod(authorized_file, 0600)
        
        sys.exit(0)
