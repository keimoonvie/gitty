import os
import sys
from gitty.command import Command
from gitty import user
from gitty import ssh
from gitty import util
from gitty.config import get_config

class DeleteKeyCommand(Command):
    def run(self):
        if len(sys.argv) < 4:
            print "Usage: %s %s [username] [keyname]" % (sys.argv[0], sys.argv[1])
            sys.exit(2)
        username = sys.argv[2]
        keyname = sys.argv[3]

        response = util.read_interactive("Delete key? [y/N]: ")
        
        if (response != 'y'):
            sys.exit(0)

        if not(user.exist(username)):
            sys.stderr.write("User not existed\n")
            sys.exit(1)

        ssh.del_key(username, keyname)
        print "SSH key deleted successfully"
        sys.exit(0)
