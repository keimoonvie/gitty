import os
import sys
from gitty.command import Command
from gitty import repo
from gitty import util

class DeleteGroupRepoCommand(Command):
    def run(self):
        if len(sys.argv) < 4:
            print "Usage: %s %s [group_name] [repo_name]" % (sys.argv[0], sys.argv[1])
            sys.exit(2)

        group_name = sys.argv[2]
        repo_name = sys.argv[3]
        repo.delete_group_from_repo(group_name, repo_name)
        print "Deleted group %s from repo %s" % (group_name, repo_name)
        sys.exit(0)
