import os
import sys
from gitty.command import Command
from gitty import group
from gitty import util

class AddUserGroupCommand(Command):
    def run(self):
        if len(sys.argv) < 4:
            print "Usage: %s %s [username] [group_name]" % (sys.argv[0], sys.argv[1])
            sys.exit(2)

        username = sys.argv[2]
        group_name = sys.argv[3]
        group.add_user_to_group(username, group_name)
        print "Added user %s to group %s" % (username, group_name)
        sys.exit(0)
