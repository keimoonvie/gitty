import os
import sys
from gitty.command import Command
from gitty import repo
from gitty import util

class AddRepoCommand(Command):
    def run(self):
        if len(sys.argv) < 3:
            self.interactive()
        else:
            self.command_line()
        self.execute()

    def interactive(self):
        self.repo_name = util.read_interactive('Repo name: ')
        self.display_name = util.read_interactive('Display name: ')
        self.description = util.read_interactive('Description: ')

    def command_line(self):
        if len(sys.argv) < 5:
            print "Usage: %s %s repo_name display_name description" % (sys.argv[0], sys.argv[1])
            sys.exit(2)

        self.repo_name = sys.argv[2]
        self.display_name = sys.argv[3]
        self.description = sys.argv[4]

    def execute(self):
        repo.add_repo(self.repo_name, self.display_name, self.description)
        print "Repo %s added successful" % self.repo_name
        sys.exit(0)
