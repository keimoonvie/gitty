import os
import sys
from gitty.command import Command
from gitty import user
from gitty import group
from gitty import repo
from gitty import util

class ViewDetailCommand(Command):
    def run(self):
        if len(sys.argv) < 4:
            print "Usage: %s %s <user|group|repo> <name>" % (sys.argv[0], sys.argv[1])
            sys.exit(2)

        detail_type = sys.argv[2]
        name = sys.argv[3]

        if detail_type == 'user':
            user.view_detail(name)
        elif detail_type == 'group':
            group.view_detail(name)
        elif detail_type == 'repo':
                repo.view_detail(name)
        else:
            print "Wrong type"
            sys.exit(1)
            
        sys.exit(0)
