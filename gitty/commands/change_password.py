import os
import sys
from gitty.command import Command
from gitty import user
from gitty import util

class ChangePasswordCommand(Command):
    def run(self):
        if len(sys.argv) < 3:
            print "Usage: % %s username" % (sys.argv[0], sys.argv[1])
            sys.exit(2)

        username = sys.argv[2]
        password = util.read_password("New password: ")
        password_retype = util.read_password("Password retype: ")

        user.change_password(username, password, password_retype)
        print "Changed password successful"
        sys.exit(0)
