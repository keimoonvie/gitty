import os
import sys
from gitty.command import Command
from gitty import group
from gitty import util

class EditGroupCommand(Command):
    def run(self):
        if len(sys.argv) < 3:
            print "Usage: %s %s [group_name]" % (sys.argv[0], sys.argv[1])
            sys.exit(2)

        group_name = sys.argv[2]

        group_data = group.load_group(group_name)

        display_name = util.read_interactive("Display name [%s]: " % group_data['display_name'])
        if len(display_name) == 0:
            display_name = group_data['display_name']

        group.edit_group(group_name, display_name)
        print "Group %s editted successfully" % group_name
        sys.exit(0)
