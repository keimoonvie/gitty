import os
import sys
from gitty.command import Command
from gitty import repo
from gitty import util

class AddUserRepoCommand(Command):
    def run(self):
        if len(sys.argv) < 4:
            print "Usage: %s %s username repo_name" % (sys.argv[0], sys.argv[1])
            sys.exit(2)

        username = sys.argv[2]
        repo_name = sys.argv[3]

        repo.add_user_to_repo(username, repo_name)
        print "Added user %s to repo %s" % (username, repo_name)
        sys.exit(0)
