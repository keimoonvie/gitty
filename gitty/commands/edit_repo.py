import os
import sys
from gitty.command import Command
from gitty import repo
from gitty import util
from gitty.config import get_config

class EditRepoCommand(Command):
    def run(self):
        if len(sys.argv) < 3:
            print "Usage: %s %s [repo_name]" % (sys.argv[0], sys.argv[1])
            sys.exit(2)
        repo_name = sys.argv[2]
        repo_data = repo.load_repo(repo_name)

        display_name = util.read_interactive("Display name [%s]: " % repo_data['display_name'])
        if len(display_name) == 0:
            display_name = repo_data['display_name']

        description = util.read_interactive("Description [%s]: " % repo_data['description'])
        if len(description) == 0:
            description = repo_data['description']

        repo.edit_repo(repo_name, description, description)
        print "Repo %s editted successfully" % repo_name
        sys.exit(0)
