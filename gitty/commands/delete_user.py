import os
import sys
from gitty.command import Command
from gitty import user
from gitty import util

class DeleteUserCommand(Command):
    def run(self):
        if len(sys.argv) < 3:
            print "Usage: %s %s [username]" % sys.argv[0]
            sys.exit(2)

        username = sys.argv[2]
        user.delete_user(username)
        print "Deleted user %s" % username
        sys.exit(0)
