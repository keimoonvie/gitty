import os
import sys
from gitty.command import Command
from gitty import user
from gitty import util
from gitty.config import get_config

class EditUserCommand(Command):
    def run(self):
        if len(sys.argv) < 3:
            print "Usage: %s %s [username]" % (sys.argv[0], sys.argv[1])
            sys.exit(2)
        username = sys.argv[2]
        
        userdata = user.load_user(username)
        
        email = util.read_interactive("Email [%s]: " % userdata['email'])
        if len(email) == 0:
            email = userdata['email']

        user.edit_user(username, email)
        print "User %s editted successfully" % username
        sys.exit(0)
