import os
import sys
from gitty.command import Command
from gitty import group
from gitty import util

class DeleteGroupCommand(Command):
    def run(self):
        if len(sys.argv) < 3:
            print "Usage: %s %s [group_name]" % sys.argv[0]
            sys.exit(2)

        group_name = sys.argv[2]
        group.delete_group(group_name)
        print "Deleted group %s" % group_name
        sys.exit(0)
