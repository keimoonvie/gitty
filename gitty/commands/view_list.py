import os
import sys
from gitty.command import Command
from gitty import user
from gitty import group
from gitty import repo
from gitty import util

class ViewListCommand(Command):
    def run(self):
        if len(sys.argv) < 3:
            print "Usage: %s %s <user|group|repo> [start] [end]" % (sys.argv[0], sys.argv[1])
            sys.exit(2)

        list_type = sys.argv[2]
        if len(sys.argv) >= 4:
            start = int(sys.argv[3])
        else:
            start = 0

        if len(sys.argv) >= 5:
            end = int(sys.argv[4])
        else:
            end = -1

        if list_type == 'user':
            user.view_list(start, end)
        elif list_type == 'group':
            group.view_list(start, end)
        elif list_type == 'repo':
            repo.view_list(start, end)
        else:
            print "Wrong list type"
            sys.exit(1)

        sys.exit(0)
