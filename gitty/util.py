import sys
import os
import pwd
import grp
import shutil
import getpass
import fcntl
import json
import re
from config import get_config
from subprocess import Popen, PIPE, STDOUT

name_re = re.compile('^[a-z0-9_\-\.]+$')
email_re = re.compile('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$')

def chown(path, user, group):
    user_info = pwd.getpwnam(user)
    uid = user_info.pw_uid
    group_info = grp.getgrnam(group)
    gid = group_info.gr_gid
    os.chown(path, uid, gid)

def chmod(path, mode):
    os.chmod(path, mode)

def create_or_clean_dir(dirname):
    user = get_config('gitty', 'gitty_user')
    group = get_config('gitty', 'gitty_group')

    if os.path.isdir(dirname):
        shutil.rmtree(dirname)
    os.makedirs(dirname)
    chown(dirname, user, group)

def delete_dir(dirname):
    shutil.rmtree(dirname)

def recreate_file(filename, data):
    user = get_config('gitty', 'gitty_user')
    group = get_config('gitty', 'gitty_group')

    f = open(filename, 'w')
    f.write(data)
    f.close()
    chown(filename, user, group)

def read_interactive(question):
    sys.stdout.write(question)
    return sys.stdin.readline().strip()

def read_password(question):
    return getpass.getpass(question)

def lock_read(f):
    fcntl.lockf(f, fcntl.LOCK_SH)

def lock_write(f):
    fcntl.lockf(f, fcntl.LOCK_EX)

def unlock_file(f):
    fcntl.lockf(f, fcntl.LOCK_UN)

def json_load(f):
    return json.load(f)

def json_dump(f, data):
    f.seek(0)
    f.truncate()
    json.dump(data, f, indent = 2)

def verify_name(name):
    return name_re.match(name)

def verify_email(email):
    return email_re.match(email)

def shell_call(cmd):
    p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    output = p.stdout.read()
    return output
