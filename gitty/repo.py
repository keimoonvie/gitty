import os
import sys
import util
import http
import user
import group
from config import get_config

def get_repo_path(repo_name):
    repo_dir = repo_name
    if repo_name[-4:len(repo_name)] != '.git':
        repo_dir = repo_name + '.git'
    return get_config('gitty', 'gitty_root') + '/repos/' + repo_dir

def init_repo(repo_name, display_name):
    repo_path = get_repo_path(repo_name)
    util.create_or_clean_dir(repo_path)
    user = get_config('gitty', 'gitty_user')
    group = get_config('gitty', 'gitty_group')
    cwd = os.getcwd()
    os.chdir(repo_path)
    util.shell_call('git init --bare')
    util.shell_call('chown -R %s:%s .' % (user, group))
    util.shell_call('mv hooks/post-update.sample hooks/post-update')
    os.chdir(cwd)
    http.init_ht_repo_conf(repo_name, display_name)

def load_repo(repo_name):
    repo_file = get_config('gitty', 'gitty_root') + '/data/repos'
    f = open(repo_file, 'r')
    util.lock_read(f)

    data = util.json_load(f)

    if not (repo_name in data):
        sys.stderr.write("Repo name not existed\n")
        sys.exit(1)

    util.unlock_file(f)
    f.close()

    return data[repo_name]

def exist(repo_name):
    repo_file = get_config('gitty', 'gitty_root') + '/data/repos'
    f = open(repo_file, 'r')
    util.lock_read(f)
    data = util.json_load(f)
    util.unlock_file(f)
    f.close()

    return repo_name in data    

def add_repo(repo_name, display_name, description):
    if not(util.verify_name(repo_name)):
        sys.stderr.write("Repo name not correct\n")
        sys.exit(1)

    repo_file = get_config('gitty', 'gitty_root') + '/data/repos'
    f = open(repo_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    if repo_name in data:
        sys.stderr.write("Repo name existed\n")
        sys.exit(1)

    init_repo(repo_name, display_name)

    data[repo_name] = {'repo_name': repo_name, 'display_name': display_name, 'description': description}
    util.json_dump(f, data)

    util.unlock_file(f)
    f.close()

def edit_repo(repo_name, display_name, description):
    repo_file = get_config('gitty', 'gitty_root') + '/data/repos'
    f = open(repo_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    if not(repo_name in data):
        sys.stderr.write("Repo name not existed\n")
        sys.exit(1)

    data[repo_name]['display_name'] = display_name
    data[repo_name]['description'] = description

    util.json_dump(f, data)

    util.unlock_file(f)
    f.close()

def add_user_to_repo(username, repo_name):
    if not(user.exist(username)):
        sys.stderr.write("Username not existed\n")
        sys.exit(1)

    repo_file = get_config('gitty', 'gitty_root') + '/data/repos'
    f = open(repo_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    if not(repo_name in data):
        sys.stderr.write("Repo name not existed\n")
        sys.exit(1)

    if not('users' in data[repo_name]):
        data[repo_name]['users'] = []

    if not(username in data[repo_name]['users']):
        data[repo_name]['users'].append(username)
    else:
        sys.stderr.write("User is already in repo\n")
        sys.exit(1)

    http.rewrite_htrepo_file(repo_name, data[repo_name])
    user.add_repo(username, repo_name)

    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()

def delete_user_from_repo(username, repo_name):
    if not(user.exist(username)):
        sys.stderr.write("Username not existed\n")
        sys.exit(1)

    repo_file = get_config('gitty', 'gitty_root') + '/data/repos'
    f = open(repo_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    if not(repo_name in data):
        sys.stderr.write("Repo name not existed\n")
        sys.exit(1)

    if not('users' in data[repo_name]):
        sys.stderr.write("User is not in repo\n")
        sys.exit(1)

    if not(username in data[repo_name]['users']):
        sys.stderr.write("User is not in repo\n")
        sys.exit(1)
    else:
        data[repo_name]['users'].remove(username)

    http.rewrite_htrepo_file(repo_name, data[repo_name])
    user.delete_repo(username, repo_name)

    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()

def add_group_to_repo(group_name, repo_name):
    if not(group.exist(group_name)):
        sys.stderr.write("Group name not existed\n")
        sys.exit(1)

    repo_file = get_config('gitty', 'gitty_root') + '/data/repos'
    f = open(repo_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    if not(repo_name in data):
        sys.stderr.write("Repo name not existed\n")
        sys.exit(1)

    if not('groups' in data[repo_name]):
        data[repo_name]['groups'] = []

    if not(group_name in data[repo_name]['groups']):
        data[repo_name]['groups'].append(group_name)
    else:
        sys.stderr.write("Group is already in repo\n")
        sys.exit(1)

    http.rewrite_htrepo_file(repo_name, data[repo_name])
    group.add_repo(group_name, repo_name)

    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()

def delete_group_from_repo(group_name, repo_name):
    if not(group.exist(group_name)):
        sys.stderr.write("Group_Name not existed\n")
        sys.exit(1)

    repo_file = get_config('gitty', 'gitty_root') + '/data/repos'
    f = open(repo_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    if not(repo_name in data):
        sys.stderr.write("Repo name not existed\n")
        sys.exit(1)

    if not('groups' in data[repo_name]):
        sys.stderr.write("Group is not in repo\n")
        sys.exit(1)

    if not(group_name in data[repo_name]['groups']):
        sys.stderr.write("Group is not in repo\n")
        sys.exit(1)
    else:
        data[repo_name]['groups'].remove(group_name)

    http.rewrite_htrepo_file(repo_name, data[repo_name])
    group.delete_repo(group_name, repo_name)

    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()

def check_user_repo(username, repo_name):
    repo_file = get_config('gitty', 'gitty_root') + '/data/repos'
    f = open(repo_file, 'r')
    util.lock_read(f)
    data = util.json_load(f)
    util.unlock_file(f)
    f.close()

    user_file = get_config('gitty', 'gitty_root') + '/data/users'
    f = open(user_file, 'r')
    util.lock_read(f)
    users_data = util.json_load(f)
    util.unlock_file(f)
    f.close()

    if not (repo_name in data):
        sys.stderr.write("Repo not found: %s\n" % repo_name)
        sys.exit(1)

    if not (username in users_data):
        sys.stderr.write("User not found: %s\n" % username)
        sys.exit(1)

    user_data = users_data[username]
    repo_data = data[repo_name]

    if 'users' in repo_data:
        if username in repo_data['users']:
            return True

    if 'groups' in repo_data and 'groups' in user_data:
        for group_name in repo_data['groups']:
            if group_name in user_data['groups']:
                return True

    return False
            

def delete_user_all(username, repos):
    repo_file = get_config('gitty', 'gitty_root') + '/data/repos'
    f = open(repo_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    for repo_name in repos:
        if not(repo_name in data):
            continue

        if not('users' in data[repo_name]):
            continue

        if username in data[repo_name]['users']:
            data[repo_name]['users'].remove(username)
            http.rewrite_htrepo_file(repo_name, data[repo_name])

    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()

def delete_group_all(group_name, repos):
    repo_file = get_config('gitty', 'gitty_root') + '/data/repos'
    f = open(repo_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    for repo_name in repos:
        if not(repo_name in data):
            continue

        if not('groups' in data[repo_name]):
            continue

        if group_name in data[repo_name]['groups']:
            data[repo_name]['groups'].remove(group_name)
            http.rewrite_htrepo_file(repo_name, data[repo_name])

    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()

def delete_repo(repo_name):
    repo_file = get_config('gitty', 'gitty_root') + '/data/repos'
    f = open(repo_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    if not(repo_name in data):
        sys.stderr.write("Repo name not existed\n")
        sys.exit(1)

    if 'users' in data[repo_name] and len(data[repo_name]['users']) > 0:
        user.delete_repo_all(repo_name, data[repo_name]['users'])

    if 'groups' in data[repo_name] and len(data[repo_name]['groups']) > 0:
        group.delete_repo_all(repo_name, data[repo_name]['groups'])

    util.delete_dir(get_repo_path(repo_name))
    http.delete_htrepo_file(repo_name)

    del data[repo_name]    
    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()

def view_list(start, end):
    repo_file = get_config('gitty', 'gitty_root') + '/data/repos'
    f = open(repo_file, 'r')
    util.lock_read(f)

    data = util.json_load(f)
    keys = data.keys()
    keys.sort()
    if end == -1:
        end = len(keys)
    elif end < -1:
        end = end + 1
    keys = keys[start:end]
    for key in keys:
        print "%s\t%s" %(data[key]['repo_name'], data[key]['display_name'])

    util.unlock_file(f)
    f.close()

def view_detail(name):
    repo_file = get_config('gitty', 'gitty_root') + '/data/repos'
    f = open(repo_file, 'r')
    util.lock_read(f)

    data = util.json_load(f)
    if not name in data:
        sys.stderr.write("Cannot find repo")
        sys.exit(1)

    repo_data = data[name]
    print "Repo name: %s" % repo_data['repo_name']
    print "Display name: %s" % repo_data['display_name']
    print "Description: %s" % repo_data['description']
    if not('users' in repo_data) or len(repo_data['users']) == 0:
        print "No user"
    else:
        print "Users: %s" % ", ".join(repo_data['users'])
    if not('groups' in repo_data) or len(repo_data['groups']) == 0:
        print "No group"
    else:
        print "Groups: %s" % ", ".join(repo_data['groups'])
    
    util.unlock_file(f)
    f.close()
