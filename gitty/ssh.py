import os
import sys
import re
import pwd
import util
from config import get_config

def get_authorized_file(user):
    user_info = pwd.getpwnam(user)
    return user_info.pw_dir + "/.ssh/authorized_keys"

def write_key(f, username, keyname, key):
    f.write('#%s %s\n' % (username, keyname))
    if (re.match('^command', key)):
        f.write(key + "\n")
    else:
        f.write('command="gitty-shell %s",no-port-forwarding,no-X11-forwarding,no-agent-forwarding,no-pty %s\n' % (username, key))

def setup_ssh_key(username, keyfile):
    if not(os.path.isfile(keyfile)):
        print "Key file not found"
        sys.exit(1)
    
    f = open(keyfile)
    key = f.readline().strip()
    f.close()

    gitty_user = get_config('gitty', 'gitty_user')
    authorized_file = get_authorized_file(gitty_user)
    f = open(authorized_file, 'a')
    util.lock_write(f)
    write_key(f, username, 'default', key)
    util.unlock_file(f)
    f.close()

def load_keys(f):
    keys = {}
    lines = f.readlines()
    for i in xrange(0, len(lines) / 2):
        username, keyname = lines[2 * i].strip("#").strip().split(' ')
        if not (username in keys):
            keys[username] = {}
        keys[username][keyname] = lines[2 * i + 1].strip()

    return keys

def save_keys(f, keys):
    f.seek(0)
    f.truncate()
    for username, userkeys in keys.iteritems():
        for keyname, key in userkeys.iteritems():
            write_key(f, username, keyname, key)

def change_key(username, keyname, keyfile):
    if not(os.path.isfile(keyfile)):
        print "Key file not found"
        sys.exit(1)

    f = open(keyfile)
    key = f.readline().strip()
    f.close()

    gitty_user = get_config('gitty', 'gitty_user')
    authorized_file = get_authorized_file(gitty_user)

    f = open(authorized_file, 'r+')
    util.lock_write(f)

    keys = load_keys(f)
    if not (username in keys):
        keys[username] = {}
    keys[username][keyname] = key
    save_keys(f, keys)

    util.unlock_file(f)
    f.close()

def del_key(username, keyname):
    gitty_user = get_config('gitty', 'gitty_user')
    authorized_file = get_authorized_file(gitty_user)

    f = open(authorized_file, 'r+')
    util.lock_write(f)

    keys = load_keys(f)
    if not (username in keys):
        sys.stderr.write("Username not found\n")
        sys.exit(1)

    if not (keyname in keys[username]):
        sys.stderr.write("Keyname not found\n")
        sys.exit(1)
        
    del keys[username][keyname]
    save_keys(f, keys)

    util.unlock_file(f)
    f.close()

def delete_all_keys(username):
    gitty_user = get_config('gitty', 'gitty_user')
    authorized_file = get_authorized_file(gitty_user)

    f = open(authorized_file, 'r+')
    util.lock_write(f)

    keys = load_keys(f)
    if username in keys:
        del keys[username]
        save_keys(f, keys)

    util.unlock_file(f)
    f.close()
