import os
import sys
import util
from config import get_config

def init_ht_repo_conf(repo_name, display_name):
    gitty_root = get_config('gitty', 'gitty_root')
    repo_dir = repo_name
    if repo_name[-4:len(repo_name)] != '.git':
        repo_dir = repo_name + '.git'
    data = """<LocationMatch %s>
\tAuthType Basic
\tAuthName "Gitty repo: %s"
\tAuthUserFile %s
\tAuthGroupFile %s
\tRequire user none_existance_user
</LocationMatch>
""" % (repo_dir, display_name, gitty_root + '/data/htusers', gitty_root + '/data/htgroups')
    util.recreate_file(gitty_root + '/data/htrepos/%s.conf' % repo_name, data.lstrip())

def change_ht_user_password(username, password):
    ht_user_file = gitty_root = get_config('gitty', 'gitty_root') + '/data/htusers'
    util.shell_call("htpasswd -bm '%s' '%s' '%s'" % (ht_user_file, username, password))

def delete_ht_user(username):
    ht_user_file = gitty_root = get_config('gitty', 'gitty_root') + '/data/htusers'
    util.shell_call("htpasswd -D '%s' '%s'" % (ht_user_file, username))

def rewrite_htgroup_file(data):
    htgroup_file = get_config('gitty', 'gitty_root') + '/data/htgroups'

    f = open(htgroup_file, 'r+')
    util.lock_write(f)
    f.seek(0)
    f.truncate()

    for group_name, group_data in data.iteritems():
        if 'users' in group_data and len(group_data['users']) > 0:
            f.write("%s: %s\n" % (group_name, " ".join(group_data['users'])))

    util.unlock_file(f)
    f.close()

def rewrite_htrepo_file(repo_name, repo_data):
    gitty_root = get_config('gitty', 'gitty_root')
    htrepo_file = gitty_root + '/data/htrepos/%s.conf' % repo_name
    repo_dir = repo_name
    if repo_name[-4:len(repo_name)] != '.git':
        repo_dir = repo_name + '.git'
    f = open(htrepo_file, 'w')
    util.lock_write(f)

    data = """<LocationMatch %s>
\tAuthType Basic
\tAuthName "Gitty repo: %s"
\tAuthUserFile %s
\tAuthGroupFile %s
""" % (repo_dir, repo_data['display_name'], gitty_root + '/data/htusers', gitty_root + '/data/htgroups')
    f.write(data)
    
    if not ('users' in repo_data):
        users = []
    else:
        users = repo_data['users']

    if not ('groups' in repo_data):
        groups =[]
    else:
        groups = repo_data['groups']

    if len(users) == 0 and len(groups) == 0:
        f.write('\tRequire user none_existance_user\n')
    else:
        if len(users) > 0:
            f.write('\tRequire user %s\n' % " ".join(users))

        if len(groups) > 0:
            f.write('\tRequire group %s\n' % " ".join(groups))
    
    f.write('</LocationMatch>')

    util.unlock_file(f)
    f.close()

def delete_htrepo_file(repo_name):
    gitty_root = get_config('gitty', 'gitty_root')
    htrepo_file = gitty_root + '/data/htrepos/%s.conf' % repo_name
    os.remove(htrepo_file)
