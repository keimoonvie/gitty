import os
import sys
import util
import ssh
import http
import repo
import group
from config import get_config

def load_user(username):
    user_file = get_config('gitty', 'gitty_root') + '/data/users'
    f = open(user_file, 'r')
    util.lock_read(f)

    data = util.json_load(f)
    
    if not (username in data):
        sys.stderr.write("Username not existed\n")
        sys.exit(1)
        
    util.unlock_file(f)
    f.close()
    
    return data[username]

def exist(username):
    user_file = get_config('gitty', 'gitty_root') + '/data/users'
    f = open(user_file, 'r')
    util.lock_read(f)
    data = util.json_load(f)
    util.unlock_file(f)
    f.close()

    return username in data

def add_user(username, email, keyfile, password, password_retype):
    if not(util.verify_name(username)):
        print "Username not correct"
        sys.exit(1)

    if not(util.verify_email(email)):
        print "Email not correct"
        sys.exit(1)

    if len(password) < 4:
        print "Password too short"
        sys.exit(1)

    if password != password_retype:
        print "Password does not match"
        sys.exit(1)

    user_file = get_config('gitty', 'gitty_root') + '/data/users'
    f = open(user_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    if username in data:
        sys.stderr.write("Username existed\n")
        sys.exit(1)

    data[username] = {'username': username, 'email': email}
    http.change_ht_user_password(username, password)

    if (keyfile):
        ssh.setup_ssh_key(username, keyfile)

    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()

def edit_user(username, email):
    if not(util.verify_email(email)):
        print "Email not correct"
        sys.exit(1)

    user_file = get_config('gitty', 'gitty_root') + '/data/users'
    f = open(user_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    if not(username in data):
        sys.stderr.write("Username not existed\n")
        sys.exit(1)

    data[username]['email'] = email

    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()

def add_group(username, group_name):
    user_file = get_config('gitty', 'gitty_root') + '/data/users'
    f = open(user_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    if not(username in data):
        sys.stderr.write("Username not existed\n")
        sys.exit(1)

    if not('groups' in data[username]):
        data[username]['groups'] = []

    if not(group_name in data[username]['groups']):
        data[username]['groups'].append(group_name)

    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()

def delete_group(username, group_name):
    user_file = get_config('gitty', 'gitty_root') + '/data/users'
    f = open(user_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    if not(username in data):
        sys.stderr.write("Username not existed\n")
        sys.exit(1)

    if not('groups' in data[username]):
        return

    if group_name in data[username]['groups']:
        data[username]['groups'].remove(group_name)

    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()

def add_repo(username, repo_name):
    user_file = get_config('gitty', 'gitty_root') + '/data/users'
    f = open(user_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    if not(username in data):
        sys.stderr.write("Username not existed\n")
        sys.exit(1)

    if not('repos' in data[username]):
        data[username]['repos'] = []

    if not(repo_name in data[username]['repos']):
        data[username]['repos'].append(repo_name)

    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()

def delete_repo(username, repo_name):
    user_file = get_config('gitty', 'gitty_root') + '/data/users'
    f = open(user_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    if not(username in data):
        sys.stderr.write("Username not existed\n")
        sys.exit(1)

    if not('repos' in data[username]):
        return

    if repo_name in data[username]['repos']:
        data[username]['repos'].remove(repo_name)

    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()

def change_password(username, password, password_retype):
    if not(exist(username)):
        sys.stderr.write("Username not existed\n")
        sys.exit(1)

    if len(password) < 8:
        print "Password too short"
        sys.exit(1)

    if password != password_retype:
        print "Password does not match"
        sys.exit(1)

    http.change_ht_user_password(username, password)

def delete_group_all(group_name, users):
    user_file = get_config('gitty', 'gitty_root') + '/data/users'
    f = open(user_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)

    for username in users:
        if not(username in data):
            continue

        if not('groups' in data[username]):
            continue

        if group_name in data[username]['groups']:
            data[username]['groups'].remove(group_name)

    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()

def delete_repo_all(repo_name, users):
    user_file = get_config('gitty', 'gitty_root') + '/data/users'
    f = open(user_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)

    for username in users:
        if not(username in data):
            continue

        if not('repos' in data[username]):
            continue

        if repo_name in data[username]['repos']:
            data[username]['repos'].remove(repo_name)

    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()

def delete_user(username):
    user_file = get_config('gitty', 'gitty_root') + '/data/users'
    f = open(user_file, 'r+')
    util.lock_write(f)

    data = util.json_load(f)
    if not(username in data):
        sys.stderr.write("Username not existed\n")
        sys.exit(1)

    if 'groups' in data[username] and len(data[username]['groups']) > 0:
        group.delete_user_all(username, data[username]['groups'])

    if 'repos' in data[username] and len(data[username]['repos']) > 0:
        repo.delete_user_all(username, data[username]['repos'])

    http.delete_ht_user(username)
    ssh.delete_all_keys(username)

    del data[username]
    util.json_dump(f, data)
    util.unlock_file(f)
    f.close()

def view_list(start, end):
    user_file = get_config('gitty', 'gitty_root') + '/data/users'
    f = open(user_file, 'r')
    util.lock_read(f)

    data = util.json_load(f)
    keys = data.keys()
    keys.sort()
    if end == -1:
        end = len(keys)
    elif end < -1:
        end = end + 1
    keys = keys[start:end]
    for key in keys:
        print "%s\t%s" % (data[key]['username'], data[key]['email'])

    util.unlock_file(f)
    f.close()

def view_detail(name):
    user_file = get_config('gitty', 'gitty_root') + '/data/users'
    f = open(user_file, 'r')
    util.lock_read(f)

    data = util.json_load(f)
    if not name in data:
        sys.stderr.write("Cannot find user")
        sys.exit(1)

    user_data = data[name]
    print "Username: %s" % user_data['username']
    print "Email: %s" % user_data['email']
    if not('groups' in user_data) or len(user_data['groups']) == 0:
        print "No group"
    else:
        print "Groups: %s" % ", ".join(user_data['groups'])
    if not('repos' in user_data) or len(user_data['repos']) == 0:
        print "No repo"
    else:
        print "Repos: %s" % ", ".join(user_data['repos'])

    util.unlock_file(f)
    f.close()
