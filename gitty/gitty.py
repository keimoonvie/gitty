#!/usr/bin/env python

import sys
import os
import config
from command import *
from commands import *

def main():
    config.config_init()

    cf = CommandFactory()
    cf.add_command('init', InitCommand(), 'Initialize gitty')

    cf.add_command('add-user', AddUserCommand(), 'Add user')
    cf.add_command('edit-user', EditUserCommand(), 'Edit user')
    cf.add_command('add-user-group', AddUserGroupCommand(), 'Add user to group')
    cf.add_command('delete-user-group', DeleteUserGroupCommand(), 'Delete user from group')
    cf.add_command('change-password', ChangePasswordCommand(), 'Change password')
    cf.add_command('add-user-repo', AddUserRepoCommand(), 'Add user to repo')
    cf.add_command('delete-user-repo', DeleteUserRepoCommand(), 'Delete user from repo')
    cf.add_command('delete-user', DeleteUserCommand(), 'Delete user from repo')

    cf.add_command('change-key', ChangeKeyCommand(), 'Change ssh key')
    cf.add_command('delete-key', DeleteKeyCommand(), 'Delete ssh key')

    cf.add_command('add-group', AddGroupCommand(), 'Add user group')
    cf.add_command('edit-group', EditGroupCommand(), 'Edit user group')
    cf.add_command('add-group-repo', AddGroupRepoCommand(), 'Add group to repo')
    cf.add_command('delete-group-repo', DeleteGroupRepoCommand(), 'Delete group from repo')
    cf.add_command('delete-group', DeleteGroupCommand(), 'Delete group')

    cf.add_command('add-repo', AddRepoCommand(), 'Add repo')
    cf.add_command('edit-repo', EditRepoCommand(), 'Edit repo')
    cf.add_command('delete-repo', DeleteRepoCommand(), 'Delete repo')

    cf.add_command('view-list', ViewListCommand(), 'View list')
    cf.add_command('view-detail', ViewDetailCommand(), 'View detail')

    sys.exit(cf.run())

if __name__ == '__main__':
    main()
